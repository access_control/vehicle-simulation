#! /bin/bash
echo "Executing special vehicle simulation based profile"

# Can be set to one of (from highest to lowest): OFF, SEVERE, WARNING, INFO, CONFIG, FINE, FINER, FINEST, ALL
#LOG_LEVEL=SEVERE

# Log format as in java.util.Formatter
# level, date, source class name, source method name, logger, message, thrown
#LOG_FORMAT='"[%1$S %2$tY-%2$tm-%2$td %2$tT:%2$tL]: {%3$%s}#{%4$%s} ==> %6$%s %7$%s"'

#CATALINA_OPTS="-Dgroup.tba.vehicle.level=$LOG_LEVEL"
#CATALINA_OPTS="$CATALINA_OPTS -Dgroup.tba.vehicle.format=$LOG_FORMAT"
#export CATALINA_OPTS

export CATALINA_PID=$CATALINA_BASE/tomcat.pid


