#!/bin/bash
############################################
# TODO must be set if it is not done
# export JAVA_HOME=
# export CATALINA_HOME= 
# MAVEN_HOME=
############################################

# Not neccessary to set this variable
# VEHICLE_SIMULATION_PROJECT_PATH=

if [ ! -v JAVA_HOME ]; then
    echo "The environment variable JAVA_HOME is not set (JDK may also be not installed)"
    exit -1
fi

if [ ! -v CATALINA_HOME ]; then
    echo "The environment variable CATALINA_HOME is not set (Tomcat may also be not installed)"
    exit -1
fi

export CATALINA_BASE=`pwd`

folders=(lib logs temp webapps work)

for folder in ${folders[@]}; do
	if [ -d $folder ]; then
		rm -r $folder
	fi	
	mkdir $folder
done

if [ ! -v VEHICLE_SIMULATION_PROJECT_PATH ]; then
    VEHICLE_SIMULATION_PROJECT_PATH=$(dirname $CATALINA_BASE)
fi

if [ -v MAVEN_HOME ]; then
    cd $MAVEN_HOME/bin
    ./mvn -f "$VEHICLE_SIMULATION_PROJECT_PATH" clean package
else
    cd $VEHICLE_SIMULATION_PROJECT_PATH
    mvn clean package
fi

if [ $? != 0 ]; then
exit $?
fi

cp $VEHICLE_SIMULATION_PROJECT_PATH/target/*.war $CATALINA_BASE/webapps

cd $CATALINA_HOME/bin

./catalina.sh run

