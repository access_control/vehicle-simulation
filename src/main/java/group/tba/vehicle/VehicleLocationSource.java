package group.tba.vehicle;

import group.tba.vehicle.movement.VehicleMovement;
import group.tba.vehicle.util.TurnCircle;
import group.tba.vehicle.util.Vector2D;
import group.tba.vehicle.util.VehicleMovementUtils;

public class VehicleLocationSource {
	
	private final VehicleData vehicle;
	private final VehicleMovement vehicleMovement;
	private final double samplePeriod;
	
	private final double duration, speed, turnRadius, effectiveSpeed;
	private final TurnCircle turnCircle;
	private final VehicleLocation vehicleLocation;
	private final Vector2D direction, position, turnCenter;
	private double currentDuration = 0.0d;
	private boolean notLastLocation = true, firstLocation = true;
	
	private VehicleLocation previousLocation;

	public VehicleLocationSource(VehicleData vehicle, VehicleMovement vehicleMovement, double samplePeriod) {
		if(samplePeriod <= 0.0d) {
			throw new IllegalArgumentException(String.format("The sample period is %f, but must be greater than 0", samplePeriod));
		}
		this.vehicle = vehicle;
		this.vehicleMovement = vehicleMovement;
		this.samplePeriod = samplePeriod;
		this.duration = vehicleMovement.getDuration();
		this.speed = vehicleMovement.getSpeed();	
		this.vehicleLocation = vehicle.getLocation();
		this.direction = vehicleLocation.getDirection();
		this.position = vehicleLocation.getPosition();
		this.turnCircle = VehicleMovementUtils.computeTurnCircle(vehicle, vehicleMovement);
		if(this.turnCircle != null) {
			this.turnCenter = this.turnCircle.getCenter();
			this.turnRadius = this.turnCircle.getRadius();
			this.effectiveSpeed = -Math.signum(this.vehicleMovement.getFrontWheelAngle()) * this.speed;
		} else {
			this.turnCenter = null;
			this.turnRadius = 0.0d;
			this.effectiveSpeed = 0.0d;
		}
	}

	public VehicleLocation getNextVehicleLocation() {
		VehicleLocation newVehicleLocation;
		if(this.firstLocation) {
			this.vehicle.setFrontWheelAngle(this.vehicleMovement.getFrontWheelAngle());
			this.firstLocation = false;
		}
		if(Math.abs(this.speed) < Double.MIN_VALUE) {
			this.previousLocation = this.vehicleLocation;
			return null;
		}
		if(this.turnCircle == null) {
			if(this.duration <= 0.0d || this.currentDuration < this.duration) {
				this.previousLocation = newVehicleLocation = new VehicleLocation(this.position.add(this.direction.multiplyBy(this.speed * this.currentDuration)), this.direction);
				this.currentDuration += this.samplePeriod;
			} else if(this.duration > 0.0d && this.currentDuration >= this.duration && this.notLastLocation) {
				this.previousLocation = newVehicleLocation = new VehicleLocation(this.position.add(this.direction.multiplyBy(this.speed * this.duration)), this.direction);
				this.notLastLocation = false;
			} else {
				newVehicleLocation = null;				
			}
		} else {
			double currentTurnAngle;
			if(this.duration <= 0.0d || this.currentDuration < this.duration) {
				currentTurnAngle = this.effectiveSpeed * this.currentDuration / this.turnRadius;
				this.previousLocation = newVehicleLocation = new VehicleLocation(this.position.sub(this.turnCenter).rotate(currentTurnAngle).add(this.turnCenter), this.direction.rotate(currentTurnAngle));
				this.currentDuration += this.samplePeriod;
			} else if(this.duration > 0.0d && this.currentDuration >= this.duration && this.notLastLocation) {
				currentTurnAngle = this.effectiveSpeed * this.duration / this.turnRadius;
				this.previousLocation = newVehicleLocation = new VehicleLocation(this.position.sub(this.turnCenter).rotate(currentTurnAngle).add(this.turnCenter), this.direction.rotate(currentTurnAngle));
				this.notLastLocation = false;
			} else {
				newVehicleLocation = null;
			}			
		}
		return newVehicleLocation;
	}

	public VehicleLocation getPreviousLocation() {
		return previousLocation;
	}
	
	public VehicleMovement getVehicleMovement() {
		return vehicleMovement;
	}
}
