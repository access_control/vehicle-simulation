package group.tba.vehicle;

import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

import group.tba.vehicle.commands.VehicleCommand;
import group.tba.vehicle.movement.VehicleMovement;
import group.tba.vehicle.movement.VehicleMovementManager;

public class VehicleManager {
	
	private double updatePeriod;
	private Map<Long, Vehicle> vehicles = new ConcurrentHashMap<>();
	private BlockingQueue<VehicleResponse> queueToClients = new LinkedBlockingQueue<>();
	private BlockingQueue<VehicleCommand> queueFromClients = new LinkedBlockingQueue<>();
	private VehicleMovementManager vehicleMovementManager;
	
	public VehicleManager(double updatePeriod) {
		this.updatePeriod = updatePeriod;
		this.vehicleMovementManager = new VehicleMovementManager(this.queueToClients, this.updatePeriod);
	}
	
	public BlockingQueue<VehicleCommand> getQueueFromClients() {
		return queueFromClients;
	}
	
	public BlockingQueue<VehicleResponse> getQueueToClients() {
		return queueToClients;
	}

	public VehicleMovementManager getVehicleMovementManager() {
		return vehicleMovementManager;
	}
	
	public Vehicle getVehicle(long id) {
		return this.vehicles.get(id);
	}
	
	public void addVehicle(Vehicle vehicle) {
		this.vehicles.putIfAbsent(vehicle.getId(), vehicle);
	}
	
	public void removeVehicle(long vehicleId) {
		Vehicle vehicle = this.vehicles.remove(vehicleId);
		if(vehicle != null) {
			this.vehicleMovementManager.removeVehicle(vehicle);
		}
	}
	
	public List<Vehicle> getVehicles() {
		return this.vehicles.values().stream().collect(Collectors.toList());
	}
	
	public List<VehicleContainer> getVehicleContainers() {
		Map<Long, VehicleMovement> vehicleMovements = this.vehicleMovementManager.getVehicleMovements();
		VehicleMovement defaultVehicleMovement = new VehicleMovement(0.0d, 0.0d, 0.0d);
		return vehicles.entrySet().stream().map(entry -> new VehicleContainer(entry.getValue(), vehicleMovements.getOrDefault(entry.getKey(), defaultVehicleMovement))).collect(Collectors.toList());
	}
	
	public void close() {
		this.vehicleMovementManager.close();
	}
	
}
