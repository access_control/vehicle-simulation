package group.tba.vehicle.movement;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import group.tba.vehicle.Vehicle;
import group.tba.vehicle.VehicleContainer;
import group.tba.vehicle.VehicleData;
import group.tba.vehicle.VehicleLocation;
import group.tba.vehicle.VehicleLocationSource;
import group.tba.vehicle.VehicleResponse;
import group.tba.vehicle.util.ThreadBlocker;
import group.tba.vehicle.util.ThreadBlocker.AwaitStatus;

public class VehicleMovementExecutor implements Runnable {
	
	private static final Logger LOG = Logger.getLogger(VehicleMovementExecutor.class.getName());
	
	private CountDownLatch latch = new CountDownLatch(1);
	private AtomicBoolean stopFlag = new AtomicBoolean();
	private ThreadBlocker threadBlocker = new ThreadBlocker();
	
	private Vehicle vehicle;
	private VehicleLocationSource vehicleLocationSource;
	private BlockingQueue<VehicleResponse> queueToClients;
	private double updatePeriod;
		

	public VehicleMovementExecutor(Vehicle vehicle, VehicleLocationSource vehicleLocationSource,
			BlockingQueue<VehicleResponse> queueToClients, double updatePeriod) {
		this.vehicle = vehicle;
		this.vehicleLocationSource = vehicleLocationSource;
		this.queueToClients = queueToClients;
		this.updatePeriod = updatePeriod;
	}
	
	public VehicleData getVehicle() {
		return vehicle;
	}
	
	public VehicleLocationSource getVehicleLocationSource() {
		return vehicleLocationSource;
	}

	public double getUpdatePeriod() {
		return updatePeriod;
	}

	@Override
	public void run() {
		LOG.info(() -> String.format("VehicleMovementExecutor [vehicle-id=%d] started", this.vehicle.getId()));
		try {
			final long periodMillis = Double.valueOf(this.updatePeriod).longValue();
			final int periodNanos = Double.valueOf(((this.updatePeriod - (long) this.updatePeriod)) * 1000000.0d).intValue();
			AwaitStatus awaitStatus = null;
			while(true) {
				if(this.stopFlag.get()) {
					break;
				}
				VehicleLocation currentLocation = this.vehicleLocationSource.getNextVehicleLocation();
				if(currentLocation == null) {
					this.queueToClients.add(new VehicleResponse(new VehicleContainer(this.vehicle.clone(), this.vehicleLocationSource.getVehicleMovement())));
					break;
				}
				if(awaitStatus != null) {
					awaitStatus = this.threadBlocker.await(periodMillis, periodNanos);		
				} else {
					awaitStatus = AwaitStatus.TIMEOUT;
				}
				this.vehicle.setLocation(currentLocation);	
				switch(awaitStatus) {
				case TIMEOUT:
					this.queueToClients.add(new VehicleResponse(new VehicleContainer(this.vehicle.clone(), this.vehicleLocationSource.getVehicleMovement())));
					break;
				case NOTIFIED:
					continue;
				case INTERRUPTED:
					throw new InterruptedException();
				}				
			}
		} catch(Throwable t) {
			t.printStackTrace();
		} finally {
			latch.countDown();
		}
		LOG.info(() -> String.format("VehicleMovementExecutor [vehicle-id=%d] ended", this.vehicle.getId()));
	}
	
	public void waitUntilFinished() {
		try {
			this.latch.await();
		} catch (InterruptedException e) {
			LOG.log(Level.SEVERE, "", e);
		}
	}
	
	public void stop() {
		this.stopFlag.set(true);
		synchronized(this.vehicle) {
			this.vehicle.notify();
		}
	}

}
