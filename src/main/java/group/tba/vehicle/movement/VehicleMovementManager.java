package group.tba.vehicle.movement;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import group.tba.vehicle.Vehicle;
import group.tba.vehicle.VehicleLocationSource;
import group.tba.vehicle.VehicleResponse;

public class VehicleMovementManager {
	
	private ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(100, 150, 50L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
	private Map<Long, VehicleMovementExecutor> executors = new ConcurrentHashMap<>();
	private BlockingQueue<VehicleResponse> queueToClients;
	private double updatePeriod;
	
	public VehicleMovementManager(BlockingQueue<VehicleResponse> queueToClients, double updatePeriod) {
		this.queueToClients = queueToClients;
		this.updatePeriod = updatePeriod;
	}

	public void addMovement(Vehicle vehicle, VehicleMovement vehicleMovement) {
		VehicleLocationSource vehicleLocationSource = new VehicleLocationSource(vehicle, vehicleMovement, this.updatePeriod);
		synchronized(vehicle) {
			VehicleMovementExecutor vehicleMovementExecutor = new VehicleMovementExecutor(vehicle, vehicleLocationSource, this.queueToClients, this.updatePeriod);
			VehicleMovementExecutor currentExecutor = this.executors.put(vehicle.getId(), vehicleMovementExecutor);
			if(currentExecutor != null) {
				currentExecutor.stop();
				currentExecutor.waitUntilFinished();				
			}
			this.threadPoolExecutor.submit(vehicleMovementExecutor);
		}
	}
	
	public void removeVehicle(Vehicle vehicle) {
		synchronized(vehicle) {
			VehicleMovementExecutor currentExecutor = this.executors.remove(vehicle.getId());
			if(currentExecutor != null) {
				currentExecutor.stop();
				currentExecutor.waitUntilFinished();				
			}
		}
	}
	
	public void close() {
		this.threadPoolExecutor.shutdown();
		this.executors.values().forEach(executor -> executor.stop());
	}
	
	public Map<Long, VehicleMovement> getVehicleMovements() {
		return executors.entrySet().stream().collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue().getVehicleLocationSource().getVehicleMovement()));
	}
	
}
