package group.tba.vehicle.movement;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import group.tba.vehicle.VehicleConstants;

public class VehicleMovement {
	
	private final double speed, duration, frontWheelAngle;
	
	@JsonCreator
	public VehicleMovement(
			@JsonProperty("speed") double speed,
			@JsonProperty("duration") double duration,
			@JsonProperty("frontWheelAngle") double frontWheelAngle) {
		VehicleConstants.checkSpeed(speed);
		VehicleConstants.checkFrontWheelAngle(frontWheelAngle);
		this.speed = speed;
		this.duration = duration;
		this.frontWheelAngle = frontWheelAngle;
	}
	
	public double getSpeed() {
		return speed;
	}

	public double getDuration() {
		return duration;
	}
	
	public double getFrontWheelAngle() {
		return frontWheelAngle;
	}
	
	public double computeDistance() {
		return this.speed * this.duration;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VehicleMovement [speed=").append(speed).append(", duration=").append(duration)
				.append(", frontWheelAngle=").append(frontWheelAngle).append("]");
		return builder.toString();
	}	
}
