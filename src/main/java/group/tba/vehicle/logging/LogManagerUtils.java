package group.tba.vehicle.logging;

import java.util.logging.LogManager;

public final class LogManagerUtils {
	public static String getLogManagerProperty(String propertyName, String defaultValue) {
		String propertyValue = LogManager.getLogManager().getProperty(propertyName);
		return propertyValue != null ? propertyValue : defaultValue;
	}
}
