
package group.tba.vehicle.logging;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.time.ZonedDateTime;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class LoggerFormatter extends Formatter {

	public static final String DEFAULT_LOG_FORMAT = "[%1$S %2$tY-%2$tm-%2$td %2$tT:%2$tL]: {%3$s}#{%4$s} ==> %6$s %7$s";
		
	private final String format;
	
	public LoggerFormatter(String format) {
		this.format = format;
	}
	
	@Override
	public String format(LogRecord record) {
		Throwable throwable = record.getThrown();
		ByteArrayOutputStream byteArrayOutputStream;
		if(throwable != null) {
			byteArrayOutputStream = new ByteArrayOutputStream();
			PrintWriter printWriter = new PrintWriter(byteArrayOutputStream);
			throwable.printStackTrace(printWriter);
			printWriter.flush();
		} else {
			byteArrayOutputStream = null;
		}
		return String.format(this.format, 
				record.getLevel(),
				ZonedDateTime.now(), 
				record.getSourceClassName(), 
				record.getSourceMethodName(), 
				record.getLoggerName(), 
				record.getMessage(),
				(byteArrayOutputStream != null ? byteArrayOutputStream.toString() : ""));
	}
}
