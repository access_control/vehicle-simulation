package group.tba.vehicle.logging;

import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.StreamHandler;

public class VehicleLogConsoleHandler extends StreamHandler {
	
	private static final String FORMAT = VehicleLogConsoleHandler.class.getName() + ".format";
	private static final String LEVEL = VehicleLogConsoleHandler.class.getName() + ".level";
	private static final String DEFAULT_LEVEL = "INFO";
	
	public VehicleLogConsoleHandler() {
		super(System.out, new LoggerFormatter(LogManagerUtils.getLogManagerProperty(FORMAT, LoggerFormatter.DEFAULT_LOG_FORMAT)));
		setLevel(Level.parse(LogManagerUtils.getLogManagerProperty(LEVEL, DEFAULT_LEVEL)));
	}

	@Override 
	public synchronized void publish(LogRecord record) {
	  super.publish(record); 
	  flush(); 
	}
	  
	@Override public synchronized void close() throws SecurityException {
	  flush();
	}	
}
