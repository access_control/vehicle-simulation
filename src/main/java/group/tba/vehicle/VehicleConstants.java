package group.tba.vehicle;

public interface VehicleConstants {
	double MAX_FRONT_WHEEL_ANGLE = Math.PI / 2.0d; // 90 degree
	double MAX_FRONT_WHEEL_ANGLE_LEFT = -MAX_FRONT_WHEEL_ANGLE;
	double MAX_FRONT_WHEEL_ANGLE_RIGHT = MAX_FRONT_WHEEL_ANGLE;
	double MAX_SPEED_FORWARD = 50.0d; // cm/ms
	double MAX_SPEED_BACKWARD = -20.0d; // cm/ms
	double MIN_WHEELBASE = 5.0d; // cm
	
	double UPDATE_PERIOD = 50.0d; // ms
	
	String VEHICLE_CREATE = "VEHICLE_CREATE";
	String VEHICLE_REMOVE = "VEHICLE_REMOVE";
	String VEHICLE_MOVE = "VEHICLE_MOVE";
	
	static void checkFrontWheelAngle(double frontWheelAngle) throws IllegalArgumentException {
		if(Math.abs(frontWheelAngle) > MAX_FRONT_WHEEL_ANGLE)
			throw new IllegalArgumentException(String.format("The front wheel angle is %f but must be between %f and %f", frontWheelAngle, MAX_FRONT_WHEEL_ANGLE_LEFT, MAX_FRONT_WHEEL_ANGLE_RIGHT));
	}
	
	static void checkSpeed(double speed) throws IllegalArgumentException {
		if(speed < MAX_SPEED_BACKWARD || speed > MAX_SPEED_FORWARD)
			throw new IllegalArgumentException(String.format("The speed is %f but must be between %f and %f", speed, MAX_SPEED_BACKWARD, MAX_SPEED_FORWARD));
	}
	
	static void checkWheelbase(double wheelbase) throws IllegalArgumentException {
		if(wheelbase < MIN_WHEELBASE)
			throw new IllegalArgumentException(String.format("The wheel base is %f but must be at least %f", wheelbase, MIN_WHEELBASE));
	}
	
	static void checkVehicleWidth(double width) throws IllegalArgumentException {
		if(width < MIN_WHEELBASE)
			throw new IllegalArgumentException(String.format("The vehicle width is %f but must be at least %f", width, MIN_WHEELBASE));
	}
}
