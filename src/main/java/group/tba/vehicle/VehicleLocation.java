package group.tba.vehicle;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import group.tba.vehicle.util.Vector2D;

public class VehicleLocation {
	public final Vector2D position;
	public final Vector2D direction;
	
	@JsonCreator
	public VehicleLocation(
			@JsonProperty("position") Vector2D position,
			@JsonProperty("direction") Vector2D direction) {
		if(Math.abs(direction.getX()) < Double.MIN_VALUE && Math.abs(direction.getY()) < Double.MIN_VALUE) {
			throw new IllegalArgumentException(String.format("The provided direction vector %s is a 0 vector", direction));
		}
		this.position = position;
		this.direction = direction.computeNormalized();
	}

	public Vector2D getPosition() {
		return position;
	}

	public Vector2D getDirection() {
		return direction;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VehicleLocation [position=").append(position).append(", direction=").append(direction)
				.append("]");
		return builder.toString();
	}
}