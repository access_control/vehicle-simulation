package group.tba.vehicle.util;

import java.util.ArrayList;
import java.util.List;

import group.tba.vehicle.VehicleData;
import group.tba.vehicle.VehicleLocation;
import group.tba.vehicle.movement.VehicleMovement;

public final class VehicleMovementUtils {
	
	public static double computeTurnRadius(double wheelBase, double frontWheelAngle) {
		final double absFrontWheelAngle = Math.abs(frontWheelAngle);
		if(absFrontWheelAngle > Double.MIN_NORMAL) {
			return wheelBase / Math.sin(absFrontWheelAngle);
		} else {
			return Double.POSITIVE_INFINITY;
		}
	}
	
	private static TurnCircle computeTurnCircle(double wheelbase, double frontWheelAngle, VehicleLocation location) {
		double turnRadius = computeTurnRadius(wheelbase, frontWheelAngle);
		TurnCircle turnCircle;
		if(Double.isFinite(turnRadius)) {
			if(frontWheelAngle > 0.0) {
				turnCircle = new TurnCircle(location.getDirection().computeRightPerpendicular().multiplyBy(turnRadius).add(location.getPosition()), turnRadius);
			} else {
				turnCircle = new TurnCircle(location.getDirection().computeLeftPerpendicular().multiplyBy(turnRadius).add(location.getPosition()), turnRadius);
			}
		} else {
			turnCircle = null;
		}
		return turnCircle;
	}
	
	public static TurnCircle computeTurnCircle(VehicleData vehicle) {
		return computeTurnCircle(vehicle.getWheelbase(), vehicle.getFrontWheelAngle(), vehicle.getLocation());
	}
	
	public static TurnCircle computeTurnCircle(VehicleData vehicle, VehicleMovement vehicleMovement) {
		return computeTurnCircle(vehicle.getWheelbase(), vehicleMovement.getFrontWheelAngle(), vehicle.getLocation());
	}
	
	public static List<VehicleLocation> computeTransitions(VehicleData vehicle, VehicleMovement vehicleMovement, double samplePeriod) {
		if(samplePeriod <= 0.0d) {
			throw new IllegalArgumentException(String.format("The sample period is %f, but must be greater than 0", samplePeriod));
		}
		vehicle.setFrontWheelAngle(vehicleMovement.getFrontWheelAngle());
		final double duration = vehicleMovement.getDuration(), speed = vehicleMovement.getSpeed();
		if(duration <= 0.0d) {
			return null;
		}
		final TurnCircle turnCircle = computeTurnCircle(vehicle);
		final VehicleLocation vehicleLocation = vehicle.getLocation();
		final Vector2D direction = vehicleLocation.getDirection(), position = vehicleLocation.getPosition();
		final List<VehicleLocation> vehicleLocations = new ArrayList<>();
	
		if(turnCircle == null) {
			for(double currentDuration = 0.0d; currentDuration < duration; currentDuration += samplePeriod) {
				vehicleLocations.add(new VehicleLocation(position.add(direction.multiplyBy(speed * currentDuration)), direction));
			}
			vehicleLocations.add(new VehicleLocation(position.add(direction.multiplyBy(speed * duration)), direction));
		} else {
			Vector2D turnCenter = turnCircle.getCenter();
			double turnRadius = turnCircle.getRadius(), effectiveSpeed = -Math.signum(vehicle.getFrontWheelAngle()) * speed, currentTurnAngle;
			for(double currentDuration = 0.0d; currentDuration < duration; currentDuration += samplePeriod) {
				currentTurnAngle = effectiveSpeed * currentDuration / turnRadius;
				vehicleLocations.add(new VehicleLocation(position.sub(turnCenter).rotate(currentTurnAngle).add(turnCenter), direction.rotate(currentTurnAngle)));
			}
			currentTurnAngle = effectiveSpeed * duration / turnRadius;
			vehicleLocations.add(new VehicleLocation(position.sub(turnCenter).rotate(currentTurnAngle).add(turnCenter), direction.rotate(currentTurnAngle)));
		}
		return vehicleLocations;
	}
	
}
