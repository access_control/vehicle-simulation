package group.tba.vehicle.util;

public class TurnCircle {
	private final Vector2D center;
	private final double radius;
	
	public TurnCircle(Vector2D center, double radius) {
		if(radius <= 0.0d) {
			throw new IllegalArgumentException(String.format("The radius is %f but must be greater than 0", radius));
		}
		this.center = center;
		this.radius = radius;
	}
	
	public Vector2D getCenter() {
		return center;
	}
	
	public double getRadius() {
		return radius;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TurnCircle [center=").append(center).append(", radius=").append(radius).append("]");
		return builder.toString();
	}
}
