package group.tba.vehicle.util;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Vector2D {
	private final double x, y;

	@JsonCreator
	public Vector2D(
			@JsonProperty("x") double x,
			@JsonProperty("y") double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
	
	public Vector2D computeNormalized() {
		return Vector2D.computeNormalized(this.x, this.y);
	}
	
	public static Vector2D computeNormalized(double x, double y) {
		final double d = Math.hypot(x, y);
		return new Vector2D(x / d, y / d);
	}
	
	public Vector2D computeRightPerpendicular() {
		return new Vector2D(y, -x);
	}
	
	public Vector2D computeLeftPerpendicular() {
		return new Vector2D(-y, x);
	}
	
	public Vector2D multiplyBy(double value) {
		return new Vector2D(value * this.x, value * this.y);
	}
	
	public Vector2D add(Vector2D d) {
		return new Vector2D(this.x + d.x, this.y + d.y);
	}
	
	public Vector2D sub(Vector2D d) {
		return new Vector2D(this.x - d.x, this.y - d.y);
	}
	
	public Vector2D rotate(double angle) {
		return new Vector2D(this.x * Math.cos(angle) - this.y * Math.sin(angle), this.x * Math.sin(angle) + this.y * Math.cos(angle));
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Vector2D [x=").append(x).append(", y=").append(y).append("]");
		return builder.toString();
	}
	
}
