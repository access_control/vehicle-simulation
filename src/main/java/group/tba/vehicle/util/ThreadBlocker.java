package group.tba.vehicle.util;

public class ThreadBlocker {

	public enum AwaitStatus {
		NOTIFIED,
		TIMEOUT,
		INTERRUPTED
	}
		
	public ThreadBlocker() {
		this(false);
	}

	public ThreadBlocker(boolean notified) {
		this.notified = notified;
	}

	private volatile boolean notified;
	
	public AwaitStatus await() {
		return await(0L);
	}
	
	public AwaitStatus await(long millis) {
		return await(millis, 0);
	}
	
	public AwaitStatus await(long millis, int nanos) {
		long started = System.currentTimeMillis(), elapsed = 0L;
		AwaitStatus awaitStatus = AwaitStatus.NOTIFIED;
		synchronized(this) {
			while(!this.notified) {
				try {
					this.wait(millis - elapsed, nanos);
					if(this.notified) {
						break;
					}
					if((millis > 0L || nanos > 0) && (elapsed = System.currentTimeMillis() - started) >= millis) {
						awaitStatus = AwaitStatus.TIMEOUT;
						break;
					}
				} catch (InterruptedException e) {
					awaitStatus = AwaitStatus.INTERRUPTED;
					break;
				}
			}
			if(awaitStatus == AwaitStatus.NOTIFIED) {
				this.notified = false;
			}
		}
		return awaitStatus;
	}
	
	public synchronized void wakeUp() {
			this.notified = true;
			this.notify();
	}
	
	public synchronized void wakeUpAll() {
			this.notified = true;
			this.notifyAll();
	}
}
