package group.tba.vehicle;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class VehicleData {

	protected final double wheelbase;
	protected final double width;
	protected double frontWheelAngle;
	protected volatile VehicleLocation location;

	@JsonCreator
	public VehicleData(
			@JsonProperty("wheelbase") double wheelbase,
			@JsonProperty("width") double width,
			@JsonProperty("location") VehicleLocation location,
			@JsonProperty("frontWheelAngle") double frontWheelAngle) {
		VehicleConstants.checkWheelbase(wheelbase);
		VehicleConstants.checkVehicleWidth(width);
		VehicleConstants.checkFrontWheelAngle(frontWheelAngle);
		this.location = location;
		this.wheelbase = wheelbase;
		this.width = width;
		this.frontWheelAngle = frontWheelAngle;		
	}

	public double getFrontWheelAngle() {
		return frontWheelAngle;
	}

	public void setFrontWheelAngle(double frontWheelAngle) {
		VehicleConstants.checkFrontWheelAngle(frontWheelAngle);
		this.frontWheelAngle = frontWheelAngle;
	}

	public double getWheelbase() {
		return wheelbase;
	}

	public double getWidth() {
		return width;
	}

	public VehicleLocation getLocation() {
		return location;
	}

	public void setLocation(VehicleLocation location) {
		this.location = location;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VehicleData [wheelbase=").append(wheelbase).append(", width=").append(width)
				.append(", frontWheelAngle=").append(frontWheelAngle).append(", location=").append(location)
				.append("]");
		return builder.toString();
	}
}