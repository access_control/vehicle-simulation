package group.tba.vehicle.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import group.tba.vehicle.VehicleConstants;
import group.tba.vehicle.VehicleData;

public class VehicleCreateCommand extends VehicleCommand {

	private final VehicleData vehicleData;

	@JsonCreator
	public VehicleCreateCommand(@JsonProperty("vehicleData") VehicleData vehicleData) {
		super(VehicleConstants.VEHICLE_CREATE);
		this.vehicleData = vehicleData;
	}
	
	public VehicleData getVehicleData() {
		return vehicleData;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VehicleCreateCommand [vehicleData=").append(vehicleData).append(", type=").append(type)
				.append("]");
		return builder.toString();
	}

	
}
