package group.tba.vehicle.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import group.tba.vehicle.VehicleConstants;

public class VehicleRemoveCommand extends VehicleCommand {
	
	private final long vehicleId;

	@JsonCreator
	public VehicleRemoveCommand(@JsonProperty("vehicleId") long vehicleId) {
		super(VehicleConstants.VEHICLE_REMOVE);
		this.vehicleId = vehicleId;
	}

	public long getVehicleId() {
		return vehicleId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VehicleRemoveCommand [vehicleId=").append(vehicleId).append(", type=").append(type).append("]");
		return builder.toString();
	}

	
}
