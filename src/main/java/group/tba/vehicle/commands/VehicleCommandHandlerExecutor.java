package group.tba.vehicle.commands;

import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import group.tba.vehicle.Vehicle;
import group.tba.vehicle.VehicleContainer;
import group.tba.vehicle.VehicleData;
import group.tba.vehicle.VehicleManager;
import group.tba.vehicle.VehicleResponse;
import group.tba.vehicle.movement.VehicleMovement;
import group.tba.vehicle.movement.VehicleMovementManager;

public class VehicleCommandHandlerExecutor implements Runnable {
	
	private static final Logger LOG = Logger.getLogger(VehicleCommandHandlerExecutor.class.getName());

	private BlockingQueue<VehicleCommand> queueFromClients;	
	private BlockingQueue<VehicleResponse> queueToClients;
	private VehicleManager vehicleManager;
	
	private static final VehicleCommand POISON = new VehicleCommand("POISON");
	
	public VehicleCommandHandlerExecutor(VehicleManager vehicleManager) {
		this.vehicleManager = vehicleManager;
		this.queueFromClients = vehicleManager.getQueueFromClients();
		this.queueToClients = vehicleManager.getQueueToClients();
	}

	@Override
	public void run() {
		LOG.info(() -> "VehicleCommandHandlerExecutor started");
			while(true) {
				try {
					VehicleCommand vehicleCommand = this.queueFromClients.take();
					if(vehicleCommand == POISON) {
						break;
					}
					if(vehicleCommand instanceof VehicleCreateCommand) {
						VehicleCreateCommand command = (VehicleCreateCommand) vehicleCommand;
						VehicleData vehicleData = command.getVehicleData();
						Vehicle vehicle = new Vehicle(vehicleData);
						this.vehicleManager.addVehicle(vehicle);
						this.queueToClients.add(new VehicleResponse(new VehicleContainer(vehicle, new VehicleMovement(0.0d, 0.0d, 0.0d))));
					} else if(vehicleCommand instanceof VehicleMovementCommand) {
						VehicleMovementCommand command = (VehicleMovementCommand) vehicleCommand;
						Vehicle vehicle = this.vehicleManager.getVehicle(command.getVehicleId());
						VehicleMovement vehicleMovement = command.getVehicleMovement();
						VehicleMovementManager vehicleMovementManager = this.vehicleManager.getVehicleMovementManager();
						if(vehicle != null) {
							vehicleMovementManager.addMovement(vehicle, vehicleMovement);						
						}
					} else if(vehicleCommand instanceof VehicleRemoveCommand) {
						VehicleRemoveCommand command = (VehicleRemoveCommand) vehicleCommand;
						this.vehicleManager.removeVehicle(command.getVehicleId());
						this.queueToClients.add(new VehicleResponse(command.getVehicleId()));
					}
				} catch(Throwable t) {
					this.queueToClients.add(new VehicleResponse(t.getMessage()));
					LOG.log(Level.SEVERE, "Error on VehicleCommandHandlerExecutor", t);
				}
			}
		LOG.info(() -> "VehicleCommandHandlerExecutor ended");
	}
	
	public void stop() {
		this.queueFromClients.add(POISON);
	}

}
