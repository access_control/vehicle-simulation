package group.tba.vehicle.commands;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import group.tba.vehicle.VehicleConstants;
import group.tba.vehicle.movement.VehicleMovement;

public class VehicleMovementCommand extends VehicleCommand {
	
	private final long vehicleId;
	private final VehicleMovement vehicleMovement;
	
	@JsonCreator
	public VehicleMovementCommand(
			@JsonProperty("vehicleId") long vehicleId,
			@JsonProperty("vehicleMovement") VehicleMovement vehicleMovement) {
		super(VehicleConstants.VEHICLE_MOVE);
		this.vehicleId = vehicleId;
		this.vehicleMovement = vehicleMovement;
	}

	public long getVehicleId() {
		return vehicleId;
	}

	public VehicleMovement getVehicleMovement() {
		return vehicleMovement;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VehicleMovementCommand [vehicleId=").append(vehicleId).append(", vehicleMovement=")
				.append(vehicleMovement).append(", type=").append(type).append("]");
		return builder.toString();
	}	
}
