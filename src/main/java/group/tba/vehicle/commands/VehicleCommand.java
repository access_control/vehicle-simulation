package group.tba.vehicle.commands;

public class VehicleCommand {
	
	protected final String type;

	public VehicleCommand(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VehicleCommand [type=").append(type).append("]");
		return builder.toString();
	}
}
