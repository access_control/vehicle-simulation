package group.tba.vehicle;

import java.util.concurrent.atomic.AtomicLong;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Vehicle extends VehicleData implements Cloneable {
	
	private static final AtomicLong ID = new AtomicLong();
	private final long id;
	
	@JsonCreator
	public Vehicle(
			@JsonProperty("wheelbase") double wheelbase,
			@JsonProperty("width") double width,
			@JsonProperty("location") VehicleLocation location,
			@JsonProperty("frontWheelAngle") double frontWheelAngle) {
		super(wheelbase, width, location, frontWheelAngle);
		this.id = Vehicle.ID.incrementAndGet();
	}
	
	public Vehicle(VehicleData vehicleData) {
		this(vehicleData.wheelbase, vehicleData.width, vehicleData.location, vehicleData.frontWheelAngle);
	}
	
	public Vehicle clone() {
		try {
			return (Vehicle) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}
	
	public long getId() {
		return id;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Vehicle [id=").append(id).append(", wheelbase=").append(wheelbase).append(", width=")
				.append(width).append(", frontWheelAngle=").append(frontWheelAngle).append(", location=")
				.append(location).append("]");
		return builder.toString();
	}

	
}
