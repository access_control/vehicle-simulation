package group.tba.vehicle.web;

import javax.websocket.Session;

import group.tba.vehicle.VehicleConstants;
import group.tba.vehicle.VehicleManager;
import group.tba.vehicle.commands.VehicleCommandHandlerExecutor;

public final class VehicleManagerContainer {
	public static final VehicleManager VEHICLE_MANAGER = new VehicleManager(VehicleConstants.UPDATE_PERIOD);
	public static final VehicleCommandHandlerExecutor VEHICLE_COMMAND_HANDLER_EXECUTOR = new VehicleCommandHandlerExecutor(VEHICLE_MANAGER);
	public static final VehicleMovementBroadcaster VEHICLE_MOVEMENT_BROADCASTER = new VehicleMovementBroadcaster(VEHICLE_MANAGER.getQueueToClients());
	
	private static Thread vehicleCommandHandlerThread, vehicleMovementBroadcasterThread;
	
	public static void startVehicleManagement() {
		vehicleCommandHandlerThread = new Thread(VEHICLE_COMMAND_HANDLER_EXECUTOR);
		vehicleCommandHandlerThread.start();
		vehicleMovementBroadcasterThread = new Thread(VEHICLE_MOVEMENT_BROADCASTER);
		vehicleMovementBroadcasterThread.start();
	}
	
	public static void stopVehicleManagement() {
		VEHICLE_COMMAND_HANDLER_EXECUTOR.stop();
		VEHICLE_MANAGER.close();
		VEHICLE_MOVEMENT_BROADCASTER.stop();
	}
	
	public static void registerSession(Session session) {
		VEHICLE_MOVEMENT_BROADCASTER.addSession(session);
	}
	
	public static void unregisterSession(Session session) {
		VEHICLE_MOVEMENT_BROADCASTER.removeSession(session);
	}
}
