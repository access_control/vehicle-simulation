package group.tba.vehicle.web;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import group.tba.vehicle.VehicleResponse;
import group.tba.vehicle.commands.VehicleCommand;

@ServerEndpoint(value = "/vehicles", decoders = {VehicleCommandDecoder.class}, encoders = {VehicleResponseEncoder.class})
public class VehicleManagerWebSocketEndPoint {
	
	private static final Logger LOG = Logger.getLogger(VehicleManagerWebSocketEndPoint.class.getName());
	
	private static final BlockingQueue<VehicleCommand> QUEUE_FROM_CLIENTS = VehicleManagerContainer.VEHICLE_MANAGER.getQueueFromClients();

	@OnOpen
	public void onOpen(Session session) {
		LOG.info(() -> String.format("Client %s opened the session", session.getId()));
		VehicleResponse vehicleResponse = new VehicleResponse(VehicleManagerContainer.VEHICLE_MANAGER.getVehicleContainers());
		LOG.info(() -> String.format("Client %s receives on open message %s", session.getId(), vehicleResponse));
		try {
			session.getBasicRemote().sendObject(vehicleResponse);
		} catch (IOException | EncodeException e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
		VehicleManagerContainer.registerSession(session);
	}
	
	@OnClose
	public void onClose(Session session) {
		LOG.info(() -> String.format("Client %s closed the session", session.getId()));
		VehicleManagerContainer.unregisterSession(session);
	}
	
	@OnError
	public void onError(Session session, Throwable throwable) {
		LOG.log(Level.SEVERE, String.format("Error with client %s",  session.getId()), throwable);
	}
	
	@OnMessage
	public void onMessage(Session session, VehicleCommand vehicleCommand) {
		LOG.info(() -> String.format("Received vehicle command from %s: %s", session.getId(), vehicleCommand));
		QUEUE_FROM_CLIENTS.add(vehicleCommand);
	}
}
