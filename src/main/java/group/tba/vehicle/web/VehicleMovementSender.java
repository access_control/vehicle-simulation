package group.tba.vehicle.web;

import java.io.IOException;
import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.websocket.EncodeException;
import javax.websocket.Session;

import group.tba.vehicle.VehicleResponse;

public class VehicleMovementSender implements Subscriber<VehicleResponse> {
	
	private static final Logger LOG = Logger.getLogger(VehicleMovementSender.class.getName());

	private Session session;
	private Subscription subscription;
	
	public VehicleMovementSender(Session session) {
		this.session = session;
	}
	
	public void close() {
		LOG.info(() -> String.format("Subscriber %s unsubscribed", this.session.getId()));
		this.subscription.cancel();
	}

	@Override
	public void onSubscribe(Subscription subscription) {
		LOG.info(() -> String.format("Subscriber %s subscribed", this.session.getId()));
		this.subscription = subscription;
		this.subscription.request(Long.MAX_VALUE);
	}

	@Override
	public void onNext(VehicleResponse vehicleResponse) {
		LOG.info(() -> String.format("Subscriber %s receives %s", this.session.getId(), vehicleResponse));
		try {
			this.session.getBasicRemote().sendObject(vehicleResponse);
		} catch (IOException | EncodeException e) {
			throw new RuntimeException(e);
		}		
	}

	@Override
	public void onError(Throwable throwable) {
		try {
			LOG.log(Level.SEVERE, String.format("Subscriber %s has ERROR so closing the session", this.session.getId()), throwable);
			this.session.close();
		} catch (IOException e) {
			LOG.log(Level.SEVERE, "", e);
		}
	}

	@Override
	public void onComplete() {
		try {
			LOG.info(() -> String.format("Subscriber %s has COMPLETED so closing the session", this.session.getId()));
			this.session.close();
		} catch (IOException e) {
			LOG.log(Level.SEVERE, "", e);
		}
	}

}
