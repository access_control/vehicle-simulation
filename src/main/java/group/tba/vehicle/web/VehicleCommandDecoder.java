package group.tba.vehicle.web;

import javax.websocket.DecodeException;
import javax.websocket.Decoder.Text;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.websocket.EndpointConfig;

import group.tba.vehicle.VehicleConstants;
import group.tba.vehicle.commands.VehicleCommand;
import group.tba.vehicle.commands.VehicleCreateCommand;
import group.tba.vehicle.commands.VehicleMovementCommand;
import group.tba.vehicle.commands.VehicleRemoveCommand;

public class VehicleCommandDecoder implements Text<VehicleCommand> {
	
	private static final String TYPE = "type";
	
	private ObjectMapper objectMapper;
	
	public VehicleCommandDecoder() {
		this.objectMapper = new ObjectMapper();
	}

	@Override
	public void init(EndpointConfig config) {
	}

	@Override
	public void destroy() {
		
	}

	@Override
	public VehicleCommand decode(String encodedMessage) throws DecodeException {
		try {
			JsonNode jsonNode = this.objectMapper.readTree(encodedMessage);
			String type = jsonNode.get(TYPE).asText();
			switch(type) {
			case VehicleConstants.VEHICLE_CREATE:
				return this.objectMapper.readValue(encodedMessage, VehicleCreateCommand.class);
			case VehicleConstants.VEHICLE_REMOVE:
				return this.objectMapper.readValue(encodedMessage, VehicleRemoveCommand.class);
			case VehicleConstants.VEHICLE_MOVE:
				return this.objectMapper.readValue(encodedMessage, VehicleMovementCommand.class);
			default:
				throw new DecodeException(encodedMessage, "Unknown vehicle command type");
			}
		} catch (Throwable t) {
			throw new DecodeException(encodedMessage, t.getMessage(), t);
		}
	}

	@Override
	public boolean willDecode(String encodedMessage) {
		try {
		return this.objectMapper.readTree(encodedMessage).has(TYPE);
		} catch(JsonProcessingException e) {
			return false;
		}
	}

}
