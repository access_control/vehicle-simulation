package group.tba.vehicle.web;

import javax.websocket.EncodeException;
import javax.websocket.Encoder.Text;
import javax.websocket.EndpointConfig;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import group.tba.vehicle.VehicleResponse;

public class VehicleResponseEncoder implements Text<VehicleResponse> {
	
	private ObjectMapper objectMapper;
	
	public VehicleResponseEncoder() {
		this.objectMapper = new ObjectMapper();
	}

	@Override
	public void init(EndpointConfig config) {
		
	}

	@Override
	public void destroy() {
		
	}

	@Override
	public String encode(VehicleResponse vehicleResponse) throws EncodeException {
		try {
			return this.objectMapper.writeValueAsString(vehicleResponse);
		} catch (JsonProcessingException e) {
			throw new EncodeException(vehicleResponse, e.getMessage(), e);
		}
	}

}
