package group.tba.vehicle.web;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.SubmissionPublisher;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.websocket.Session;

import group.tba.vehicle.VehicleResponse;

public class VehicleMovementBroadcaster implements Runnable {
	
	private static final Logger LOG = Logger.getLogger(VehicleMovementBroadcaster.class.getName());

	private static final VehicleResponse POISON = new VehicleResponse(Collections.emptyList());
	private BlockingQueue<VehicleResponse> queueToClients;
	private SubmissionPublisher<VehicleResponse> publisher = new SubmissionPublisher<>();
	private Map<Session, VehicleMovementSender> senders = new ConcurrentHashMap<>();
	
	public VehicleMovementBroadcaster(BlockingQueue<VehicleResponse> queueToClients) {
		this.queueToClients = queueToClients;
	}

	public void addSession(Session session) {
		VehicleMovementSender sender = new VehicleMovementSender(session);
		this.senders.put(session, sender);
		this.publisher.subscribe(sender);
	}
	
	public void removeSession(Session session) {
		VehicleMovementSender sender = this.senders.remove(session);
		if(sender != null) {
			sender.close();
		}
	}

	@Override
	public void run() {
		try {
			LOG.info(() -> "VehicleMovementBroadcaster started");
			while(true) {
				VehicleResponse vehicleResponse = this.queueToClients.take();
				if(vehicleResponse == POISON) {
					this.publisher.close();
					break;
				}
				this.publisher.submit(vehicleResponse);
			}
		} catch(Throwable t) {
			LOG.log(Level.SEVERE, "", t);
		}
		LOG.info(() -> "VehicleMovementBroadcaster ended");
	}
	
	public void stop() {
		this.queueToClients.add(POISON);
	}

}
