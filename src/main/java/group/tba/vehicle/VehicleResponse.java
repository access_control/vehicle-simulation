package group.tba.vehicle;

import java.util.ArrayList;
import java.util.List;

public class VehicleResponse {
	
	private List<VehicleContainer> vehicleContainers;
	private long vehicleId = -1L;
	private String errorMessage;

	public VehicleResponse(List<VehicleContainer> vehicleContainers) {
		this.vehicleContainers = vehicleContainers;
	}

	public VehicleResponse(VehicleContainer vehicle) {
		this.vehicleContainers = new ArrayList<>();
		this.vehicleContainers.add(vehicle);
	}
	
	public VehicleResponse(long vehicleId) {
		this.vehicleId = vehicleId;
	}
	
	public VehicleResponse(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public List<VehicleContainer> getVehicleContainers() {
		return vehicleContainers;
	}
	
	public long getVehicleId() {
		return vehicleId;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VehicleResponse [vehicleContainers=").append(vehicleContainers).append(", vehicleId=")
				.append(vehicleId).append(", errorMessage=").append(errorMessage).append("]");
		return builder.toString();
	}
}
