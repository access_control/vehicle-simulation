package group.tba.vehicle;

import group.tba.vehicle.movement.VehicleMovement;

public class VehicleContainer {
	
	private final Vehicle vehicle;
	private final VehicleMovement vehicleMovement;
	
	public VehicleContainer(Vehicle vehicle, VehicleMovement vehicleMovement) {
		this.vehicle = vehicle;
		this.vehicleMovement = vehicleMovement;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public VehicleMovement getVehicleMovement() {
		return vehicleMovement;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VehicleContainer [vehicle=").append(vehicle).append(", vehicleMovement=")
				.append(vehicleMovement).append("]");
		return builder.toString();
	}	
}
