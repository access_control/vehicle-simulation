# Vehicle Simulation

* Vehicles can be created and removed during server being online, but no persistence 
  is provided. For creation you need to give wheelbase and width (each at least 5 cm),
  central position as point (x and y), direction as vector (x and y), initial front wheel angle
  in degrees (the front wheel angle can be between -90 (max left) and +90 (max right) degrees).
  The coordinates: x-axis is from left to right rising, y-axis from top to down rising. So make sure
  the vehicles you are creating are can be accessed on the screen.
* Vehicles can be selected (by click) and a move operation can be applied. Speed (cm / ms) should not exceed 
  0.1. Speed 0 means the vehicle stops. Positive values allow moving the vehicle onwards, negative backwards.
  The movement is applied during a given duration.
  If the duration (ms) is lesser than or equal to zero the duration is infinite. A new front wheel angle
  can be changed every movement application.
  
# Code exploring
 
 The actual code is not documented. Thats why the entry points for code exploring (backend) are:
 
* VehicleManagerWebSocketEndPoint
* WebVehicleSimulatorServlet
* VehicleManagerContainer
* VehicleMovementBroadcaster
* VehicleCommandHandlerExecutor
* VehicleMovementExecutor
 
# Requirements
 
* JDK >= 9 (Tested on JDK 13 and 14) (environment variable JAVA_HOME must have path to that directory) [https://adoptopenjdk.net/]()
* Maven >= 3.6.3 (Must be available in PATH or through environment variable MAVEN_HOME) [http://maven.apache.org/download.cgi](Maven)
* Bash interpreter (on windows it is recommended to have git client with bash integration) [https://git-scm.com/download/win](git for windows)
* Tomcat >= 9.0.35 ([https://tomcat.apache.org/download-90.cgi](Tomcat)). See RUNNING.txt file in the download package in order
 	how to run it.
* CATALINA_HOME must be set appropriately to Tomcat root directory
 
# Launching

After all requirements are fulfilled go to the folder "tomcat-profile" via a bash and edit the necessary environment variables in the header of
"vehicle-simulation-startup.sh", then execute it.

If you have not enough permissions execute: "chmod ug+x vehicle-simulation-startup.sh".

Via some Internet Browser go to: http://localhost:8080/vehicles-simulation.

To stop the Tomcat server press Ctrl-C in the executing console.

# Quick Deployment

The requirements now as mentioned above, but Maven must be available in PATH and BASH is not more necessary.
Go to the cloned root folder of the vehicle-simulation via command line. Execute "mvn clean package".
If the build was successful look for the file "vehicle-simulation.war" in the sub folder "target".
Copy it to $CATALINA_HOME/webapps. Go to $CATALINA_HOME/bin and execute "catalina.sh run" on *nix
or "catalina.bat run" on Windows.
 	
 
 
 